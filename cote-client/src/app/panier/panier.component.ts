import { Component, OnInit } from '@angular/core';
import { PanierService } from '../panier.service';
import { Observable } from 'rxjs';
import { AuthentificationService } from '../authentification.service';
import { ActivatedRoute,Router, Params} from  '@angular/router'; 
import { ProduitsService } from '../produits.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  private user: Observable<string>;
  private panier: String[] = new Array();
  private action: string;
  private produit;
  private email : string;
  private numAction : number;

  constructor( private panierService: PanierService, 
               private produitService: ProduitsService,
               private authService: AuthentificationService, 
               private router: Router, 
               private activatedRoute: ActivatedRoute) {

      this.user = this.authService.getUser();  
    this.email = this.user['_value'] ;
  }

  ngOnInit() {
    let monaction ;
    let monproduitId;
    this.activatedRoute.params.subscribe(params=>{
      console.log("panier ts ngOninit()" + params["action"]+ params["produitId"]);
       monaction = params["action"];
       monproduitId = params["produitId"];
      if( monaction !== undefined && monproduitId !== undefined){
      this.panierProduit(monaction, monproduitId);
      }
    });
    if( monaction == undefined && monproduitId == undefined){
      var sub =  this.panierService.getPanierProducts(this.user['_value']).subscribe(res =>{
      this.panier= res;
      console.log("mon panier :"+JSON.stringify(this.panier));
      });
      //   value => this.panier =value,
      // err=> console.log("err:"+err));



      }
      console.log("mon panier"+this.panier);
  }
/**supprimer un produit du panier d'un membre */

supprimerProduitPanier(produitId){
  let action = "remove";
  console.log("supprimerProduitPanier: id "+JSON.stringify(produitId));
  this.panierProduit(action,produitId);
  this.ngOnInit();

}

/**gerer un panier (ajouter ou supprimer un produit) */
panierProduit(action, produitId){
if(action == "add"){ this.action = "Ajout";}
if(action == "remove"){this.action = "Retrait";}
console.log("email:"+this.email);
console.log("action:"+action);
//récuperer le produit par son id
 this.produitService.getProduitById(produitId).subscribe(res => this.produit = res);
console.log("mon produit par id "+ this.produit);
//modification de notre panier stocké sur le serveur et affichage de son contenu
this.panierService.modifierPanier(action,produitId,this.email).subscribe(res=> {
  this.router.navigate(['/panier'
  // ,{
  //                                   outlets:{'app-panier':['display',this.numAction]}
  // }
]);

  this.numAction++;
});


  }


}
