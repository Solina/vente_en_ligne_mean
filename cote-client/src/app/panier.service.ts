import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class PanierService {
  private urlBase: string = 'http://localhost:8888/';

// Http Headers
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

  constructor(private http: HttpClient) { 

  }


  getPanierParMembre(membre): Observable<any> {

    return this.http.get(this.urlBase+'panier/'+membre);
}
/**
 * 
 * @param email 
 */
getPanierProductIds(email:string): Observable<any> {
  let url :string =this.urlBase +'panierProduitIds/'+email;
    let obs : Observable<any> = this.http.get(url);
return obs;
}

/**lister tout les produit d'un palier d'un clients */
getPanierProducts(parametres: string): Observable<any>{
let url= this.urlBase+"paniers/produits/"+parametres;
//let obs :Observable<any> = this.http.get(url).map((res:Response)=> res.json());
let obs :Observable<any> = this.http.get(url);
console.log("panier production par mail");
return obs;
}


/**ajouter /supprimer un produit du panier */


modifierPanier(action: string, produitId: string, email: string):Observable<any>{
// let headers = new Headers({"Contet-type":"application/json"});
// let options = new RequestOptions({headers:headers});
let observable: Observable<any>;
//ajouter un produit
if(action == "add"){
observable = this.http.post(
              this.urlBase+"paniers",
              {"produitId":produitId,"email":email},
              this.httpOptions
              );
}
// supprimer un produit:
if(action == "remove"){
  observable = this.http.delete(
    this.urlBase+"paniers/"+produitId+"/"+email,this.httpOptions );
}


  return observable;
}





}
