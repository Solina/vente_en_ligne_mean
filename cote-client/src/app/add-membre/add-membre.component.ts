import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-membre',
  templateUrl: './add-membre.component.html',
  styleUrls: ['./add-membre.component.css']
})
export class AddMembreComponent implements OnInit {
  private utilisateur = {
    "nom": "",
    "prenom": "",
    "email": "",
    "password": ""
  };

  private message: string;

  constructor(private authentificationService: AuthentificationService,
              private router : Router) { }

  ngOnInit() {
  }



  /** à la soumission fu formulaire les infos du nouveau membre a créer sont récuperées
   * dans la variable utilisateur (databinding ngModel) 
   * avant de lancer la création du nouveau memebre on vérifie si il n'existe 
   * pas déja dans le table membres sinon on l 'ajoute
   */
  onSubmit() {
    console.log(" début creér un nouveau membre...");
    //1. vérification de son existence 
    this.authentificationService.verificationConnexion(this.utilisateur).subscribe(reponse => {

      console.log("message:"+this.message);
      let resulat = reponse['resultat']
      console.log("resultat:"+ resulat);
      if (reponse['resultat']==0){
        //  this.authentificationService.connect(this.utilisateur.email);
        this.authentificationService.creerCompte(this.utilisateur).subscribe(response => {
          console.log("response:" + JSON.stringify(response));
          this.message = "compte crée avec succés"
          this.authentificationService.connect(this.utilisateur.email);
          this.router.navigate(['/categories']);
        });
      }else{
        this.message= "ce compte existe déjà!";
      }

    }
    );
  
  }
}