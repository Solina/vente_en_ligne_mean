import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root'})
export class ProduitsService {

    private urlBase: string = 'http://localhost:8888/';

    constructor( private http: HttpClient) { }

    getProduits(): Observable<any> {
        return this.http.get(this.urlBase+'produits');
    }

    getProduitsParCategorie(categorie): Observable<any> {
        return this.http.get(this.urlBase+'produits/'+categorie);
    }

    getCategories(): Observable<any> {
        return this.http.get(this.urlBase+'categories');
    }  

    getProduitById(id : string): Observable<any>{
console.log("getProduitById(): "+id);
        return this.http.get(this.urlBase+"produit/"+id);
    }
}
