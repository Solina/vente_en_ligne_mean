import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let ConnexionComponent = class ConnexionComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
        this.utilisateur = { "email": "", "password": "" };
        this.message = "";
    }
    onSubmit() {
        this.authService.verificationConnexion(this.utilisateur).subscribe(reponse => {
            this.message = reponse['message'];
            console.log("verificationConnexion:" + JSON.stringify(reponse));
            console.log(reponse['resultat']);
            if (reponse['resultat']) {
                this.authService.connect(this.utilisateur.email);
                this.router.navigate(['/categories']);
                console.log("ici");
            }
            console.log("la");
            setTimeout(() => { this.router.navigate(['/categories']); }, 1000);
        });
    }
    /** redirection vers le composant add membre */
    creerCompte() {
        this.router.navigate(['/add-membre']);
    }
};
ConnexionComponent = tslib_1.__decorate([
    Component({
        selector: 'app-connexion',
        templateUrl: './connexion.component.html',
        styleUrls: ['./connexion.component.css']
    })
], ConnexionComponent);
export { ConnexionComponent };
//# sourceMappingURL=connexion.component.js.map