import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProduitsComponent } from './produits/produits.component';
import { PanierComponent } from './panier/panier.component';
import { AddMembreComponent } from './add-membre/add-membre.component';
import { IzanComponent } from './izan/izan.component';
const routes = [
    {
        path: 'add-membre',
        component: AddMembreComponent
    },
    { path: 'membres/connexion',
        component: ConnexionComponent
    },
    { path: 'categories',
        component: CategoriesComponent
    },
    { path: 'produits/:categorie',
        component: ProduitsComponent
    },
    { path: 'produits',
        component: ProduitsComponent
    },
    {
        path: 'panier',
        component: PanierComponent
    },
    {
        path: 'panier/:action/:produitId',
        component: PanierComponent
    },
    {
        path: 'izan',
        component: IzanComponent
    }
];
// on integre les routes dans l'appli
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        //  que tu va enregisterer se trouve dans la constante route
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map