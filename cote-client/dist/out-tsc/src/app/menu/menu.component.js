import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let MenuComponent = class MenuComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
        this.user = this.authService.getUser();
    }
    ngOnInit() {
        this.router.navigate(['/categories']);
    }
    deconnexion() {
        this.authService.disconnect();
        this.router.navigate(['/categories']);
    }
    getPanier() {
        console.log("panier button menu");
        this.router.navigate(['/panier']);
    }
};
MenuComponent = tslib_1.__decorate([
    Component({
        selector: 'app-menu',
        templateUrl: './menu.component.html',
        styleUrls: ['./menu.component.css']
    })
], MenuComponent);
export { MenuComponent };
//# sourceMappingURL=menu.component.js.map