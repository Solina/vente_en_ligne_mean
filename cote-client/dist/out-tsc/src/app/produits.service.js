import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let ProduitsService = class ProduitsService {
    constructor(http) {
        this.http = http;
        this.urlBase = 'http://localhost:8888/';
    }
    getProduits() {
        return this.http.get(this.urlBase + 'produits');
    }
    getProduitsParCategorie(categorie) {
        return this.http.get(this.urlBase + 'produits/' + categorie);
    }
    getCategories() {
        return this.http.get(this.urlBase + 'categories');
    }
    getProduitById(id) {
        console.log("getProduitById(): " + id);
        return this.http.get(this.urlBase + "produit/" + id);
    }
};
ProduitsService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' })
], ProduitsService);
export { ProduitsService };
//# sourceMappingURL=produits.service.js.map