import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let CategoriesComponent = class CategoriesComponent {
    constructor(router, authService, produitsService, panierService) {
        this.router = router;
        this.authService = authService;
        this.produitsService = produitsService;
        this.panierService = panierService;
        this.categories = new Array();
        this.user = this.authService.getUser();
    }
    ngOnInit() {
        this.produitsService.getCategories().subscribe(categories => {
            this.categories = categories;
        });
        console.log("user current:" + this.user['_value']);
        /* //
        let obs : Observable<any>= this.panierService.getPanierProductIds(this.user['_value']);
              console.log("ids product panier: "+ JSON.stringify(obs));
           */
        var sub = this.panierService.getPanierProducts(this.user['_value']).subscribe(value => console.log("mes valeur:" + JSON.stringify(value)), err => console.log("err:" + err));
    }
    produitsParCategorie(categorie) {
        console.log("mon param ds ecran categories:" + categorie);
        this.router.navigate(['/produits', categorie]);
    }
    getPanierProducts() {
        var sub = this.panierService.getPanierProducts(this.user['_value']).subscribe(value => console.log("mes valeur:" + value), err => console.log("err:" + err));
        console.log("panier: " + JSON.stringify(sub));
    }
};
CategoriesComponent = tslib_1.__decorate([
    Component({
        selector: 'app-categories',
        templateUrl: './categories.component.html',
        styleUrls: ['./categories.component.css']
    })
], CategoriesComponent);
export { CategoriesComponent };
//# sourceMappingURL=categories.component.js.map