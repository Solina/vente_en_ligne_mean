import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let AddMembreComponent = class AddMembreComponent {
    constructor(authentificationService) {
        this.authentificationService = authentificationService;
        this.utilisateur = { "nom": "",
            "prenom": "",
            "email": "",
            "password": "" };
    }
    ngOnInit() {
    }
    onFormSubmit() {
        this.authentificationService.creerCompte(this.utilisateur);
    }
};
AddMembreComponent = tslib_1.__decorate([
    Component({
        selector: 'app-add-membre',
        templateUrl: './add-membre.component.html',
        styleUrls: ['./add-membre.component.css']
    })
], AddMembreComponent);
export { AddMembreComponent };
//# sourceMappingURL=add-membre.component.js.map