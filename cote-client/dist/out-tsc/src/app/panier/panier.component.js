import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let PanierComponent = class PanierComponent {
    constructor(panierService, produitService, authService, router, activatedRoute) {
        this.panierService = panierService;
        this.produitService = produitService;
        this.authService = authService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.panier = new Array();
        this.user = this.authService.getUser();
        this.email = this.user['_value'];
    }
    ngOnInit() {
        let monaction;
        let monproduitId;
        this.activatedRoute.params.subscribe(params => {
            console.log("panier ts ngOninit()" + params["action"] + params["produitId"]);
            monaction = params["action"];
            monproduitId = params["produitId"];
            if (monaction !== undefined && monproduitId !== undefined) {
                this.panierProduit(monaction, monproduitId);
            }
        });
        if (monaction == undefined && monproduitId == undefined) {
            var sub = this.panierService.getPanierProducts(this.user['_value']).subscribe(value => this.panier = value, err => console.log("err:" + err));
        }
        console.log("mon panier" + this.panier);
    }
    /**supprimer un produit du panier d'un membre */
    supprimerProduitPanier(produitId) {
        let action = "remove";
        console.log("supprimerProduitPanier: id " + JSON.stringify(produitId));
        this.panierProduit(action, produitId);
    }
    /** */
    panierProduit(action, produitId) {
        console.log("================panierProduit=======");
        if (action == "add") {
            this.action = "Ajout";
        }
        if (action == "remove") {
            this.action = "Retrait";
        }
        console.log("email:" + this.email);
        console.log("action:" + action);
        //récuperer le produit par son id
        this.produitService.getProduitById(produitId).subscribe(res => this.produit = res);
        console.log("mon produit par id " + this.produit);
        //modification de notre panier stocké sur le serveur et affichage de son contenu
        this.panierService.modifierPanier(action, produitId, this.email).subscribe(res => {
            this.router.navigate(['/panier'
                // ,{
                //                                   outlets:{'app-panier':['display',this.numAction]}
                // }
            ]);
            this.numAction++;
        });
    }
};
PanierComponent = tslib_1.__decorate([
    Component({
        selector: 'app-panier',
        templateUrl: './panier.component.html',
        styleUrls: ['./panier.component.css']
    })
], PanierComponent);
export { PanierComponent };
//# sourceMappingURL=panier.component.js.map